# osa-com-sci
Tracking the version history of my Com Sci assignments and giving me motivation to comment properly.

## Assignments

### Course Introduction
  - Introductory Assignment - How to Make a Peanut Butter Sandwich

### CSE 1010 - Computer Science 1
  - Exercise 1 - Computer Science is Changing Everything
  - Exercise 2 - Early Computing
  - Exercise 3 - Electronic Computing
  - Exercise 4 - Boolean Logic  & Logic Gates

  - Assignment 1 - What is Computer Science?
  - Assignment 2 - Who Works in Computer Science?
  - Assignment 3 - Boolean Logic
  - Assignment 4 - Von Neumann Architecture

### CSE 1110 - Structured Programming 1
  - Exercise 1 - Pixels
  - Exercise 2 - RGB
  - Exercise 3 - Error Handling
  - Exercise 4 - Creating Variables and Functions
  - Assignment 1 - Snowman
  - Assignment 2 - Etch-a-Sketch
  - Assignment 3 - Pythagoras
  - Course Project - Paint By Dot

### CSE 1120 Structured Programming 2
  - Exercise 1 - If, Else and Else If Statements
  - Exercise 2 - Logic Operators
  - Exercise 3 - While Loops
  - Assignment 1 - Big Piano
  - Assignment 2 - Pong